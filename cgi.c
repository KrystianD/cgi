#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <unistd.h>
#include "yuarel.h"

int main() {
	struct sockaddr_un svAddr, clAddr;
	char buf[100];
	int sfd, rc;

	if ((sfd = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&clAddr, 0, sizeof(clAddr));
	clAddr.sun_family = AF_UNIX;
	snprintf(clAddr.sun_path, sizeof(clAddr.sun_path) - 1, "/tmp/tmp.%ld", (long)getpid());

	if (bind(sfd, (struct sockaddr*)&clAddr, sizeof(struct sockaddr_un)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&svAddr, 0, sizeof(struct sockaddr_un));
	svAddr.sun_family = AF_UNIX;
	strcpy(svAddr.sun_path, "/tmp/socket");

	const char* qs = getenv("QUERY_STRING");

	struct yuarel_param params[2];

	const char* action = 0;
	const char* content = 0;

	int p = yuarel_parse_query(qs, '&', params, 2);
	while (p-- > 0) {
		if (strcmp(params[p].key, "action") == 0)
			action = params[p].val;
		else if (strcmp(params[p].key, "content") == 0)
			content = params[p].val;
	}

	if (action) {
		if (strcmp(action, "get-posts") == 0) {
			buf[0] = 0;
			rc = 1;
			sendto(sfd, buf, rc, 0, (struct sockaddr*)&svAddr, sizeof(struct sockaddr_un));
			int respLen = recvfrom(sfd, buf, sizeof(buf), 0, 0, 0);
			printf("\n");
			fwrite(buf, respLen, 1, stdout);
		}

		if (strcmp(action, "add-post") == 0) {
			buf[0] = 1;
			strcpy(buf + 1, content);
			rc = 1 + strlen(content) + 1;
			sendto(sfd, buf, rc, 0, (struct sockaddr*)&svAddr, sizeof(struct sockaddr_un));

			printf("Content-Type: text/html; charset=us-ascii\n");
			printf("\n");
			printf("<head> \n"
			       "  <meta http-equiv=\"refresh\" content=\"0; URL=/\" />\n"
			       "</head>");
		}
	} else {
		printf("Content-Type: text/plain; charset=us-ascii\n");
		printf("\n");
		printf("test\n");
	}

	remove(clAddr.sun_path);
	return 0;
}
