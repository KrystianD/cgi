#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <sys/stat.h>

char buf[1 * 1024 * 1024];

char* readFile() {
	FILE* f = fopen("/tmp/posts.txt", "rt");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	char* string = malloc(fsize + 1);
	fread(string, fsize, 1, f);
	fclose(f);

	string[fsize] = 0;

	return string;
}

size_t processCommand() {
	int cmd = buf[0];
	size_t len;
	char* posts;
	FILE* f;

	switch (cmd) {
		case 0:
			posts = readFile();
			len = strlen(posts);
			strcpy(buf, posts);
			free(posts);
			return len;
		case 1:
			f = fopen("/tmp/posts.txt", "at");
			fprintf(f, "---\n%s\n", buf + 1);
			fclose(f);
			return 0;
		default:
			return 0;
	}
}

int main(int argc, char* argv[]) {
	struct sockaddr_un svAddr;
	int fd;

	if ((fd = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&svAddr, 0, sizeof(svAddr));
	svAddr.sun_family = AF_UNIX;
	strcpy(svAddr.sun_path, "/tmp/socket");
	unlink("/tmp/socket");

	if (bind(fd, (struct sockaddr*)&svAddr, sizeof(svAddr)) == -1) {
		perror("bind error");
		exit(-1);
	}

	chmod("/tmp/socket", 0777);

	struct sockaddr_un clAddr;
	int clAddrLen;

	for (;;) {
		clAddrLen = sizeof(struct sockaddr_un);

		recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr*)&clAddr, &clAddrLen);
		int respLen = processCommand();

		if (respLen > 0) {
			sendto(fd, buf, respLen, 0, (struct sockaddr*)&clAddr, clAddrLen);
		}
	}
}