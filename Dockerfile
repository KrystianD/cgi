FROM ubuntu:18.04

RUN apt-get update && apt-get -y install apache2 build-essential cmake

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR

RUN mkdir -p /var/www/cgi-bin
COPY site.conf /etc/apache2/sites-available/000-default.conf

RUN ln -s /etc/apache2/mods-available/cgi.load /etc/apache2/mods-enabled/cgi.load

RUN mkdir -p /opt/app /var/www/cgi-bin/
COPY *.c *.h CMakeLists.txt entrypoint.sh /opt/app/

WORKDIR /opt/app
RUN cmake . && make && cp ./cgi /var/www/cgi-bin/

COPY index.html /var/www/html/

ENTRYPOINT "/opt/app/entrypoint.sh"
